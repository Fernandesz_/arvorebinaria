package arvorefinal;


import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		int opcao;
		NoArvore abb = null;	
		int valor;
		String nome = null;
		String selec;
		
		do {
			System.out.println("-----------------------------------------------");
			System.out.println("=	        Escolha uma opção            =");
			System.out.println("-----------------------------------------------");
			System.out.println("=	[1]. Inserir nó.                     =");
			System.out.println("=	[2]. Remover nó.                     =");
			System.out.println("=	[3]. Pesquisar nó.                   =");
			System.out.println("=	[4]. Limpar Arvore.                  =");
			System.out.println("=	[5]. Exibir Arvore.                  =");
			System.out.println("=	[0]. Sair do programa.               =");
			System.out.println("-----------------------------------------------");
			System.out.print("-> ");
			opcao = teclado.nextInt();
			switch(opcao) {
			
			case 0:
				System.out.println("Fim do programa.");
				break;
				
			case 1:
				System.out.println("Deseja inserir um RGM?");
				System.out.print("-> ");
				selec = teclado.next();
				while(selec.equals("Sim") || selec.equals("sim")){
					System.out.println("\nInforme o RGM a ser inserido:");
					System.out.print("-> ");
					valor = teclado.nextInt();
					System.out.println("\nInforme o nome a ser inserido:");
					System.out.print("-> ");
					nome = teclado.next();
					
					if(abb == null) {
						abb = new NoArvore(valor, nome);	
						System.out.println("\nPara adicionar outro RGM digite \"Sim\" ");
						System.out.print("-> ");
						teclado.nextLine();
						selec = teclado.next();
					}
					else {
						abb.insere(abb, valor, nome);
						System.out.println("\nPara adicionar outro RGM digite \"Sim\" ");
						System.out.print("-> ");
						teclado.nextLine();
						selec = teclado.next();
					}
				}
				break;
				
			case 2:
				if(abb == null) {
					System.out.println("Arvore vazia.");
				}else {
					System.out.println("Informe o RGM a ser removido:");
					System.out.print("-> ");
					valor = teclado.nextInt();
					abb.remove(abb, valor);
					System.out.println("Acessando o no apagado: ");
					System.out.println("Matricula: " + valor + " | Nome: " + nome);
				}
				break;
			
			case 3:
				if(abb == null) {
					System.out.println("Arvore vazia.");
				}
				System.out.println("\n Informe o RGM a ser pesquisado.");
				System.out.print("->");
				
				valor = teclado.nextInt();

				NoArvore resultado = abb.busca(abb, valor, nome);
				if(resultado == null) {
					System.out.println("Valor não encontrado.");
				}
				else {
					System.out.println("Valor encontrado. Acessesando nó: ");
					System.out.println("Matricula: " + resultado.valor +
							       " | Nome: " + resultado.nome);
				}
				break;
			
			case 4:
				if(abb == null){
					System.out.println("Árvore já está vazia.");
				}
				else {
					abb.limparArvore();
					abb = null;
					System.out.println("Ávore esvaziada.");
				}

				break;
			
			case 5:
				if(abb == null){
					System.out.println("Árvore vazia.");
				}
				else {
				abb.caminhar(abb);
				}
				break;
			
			default:
				System.out.println("Opção inválida.");
				break;

			}
		}while(opcao != 0);
		teclado.close();
	}
}
